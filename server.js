const mongoose = require('mongoose')
const dotenv = require('dotenv')

process.on('uncaughtException', err => {
    console.log('UNHANDLED EXCEPTION! 🔥 shutting down...')
    console.log(err.name, err.message)
    process.exit(1)
})

dotenv.config({ path: './config.env' })
//Import the express related module
const app = require('./app')

//CONNECT DB:
const DB = process.env.DATABASE
mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(() => console.log('DB connection successful!'))


// START SERVER
const port = process.env.PORT || 3000
const server = app.listen(port, () => {
    console.log(`Server listening at port ${port}...`)
})

process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION! 🔥 shutting down...')
    console.log(err.name, err.message)
    server.close(() => {
        process.exit(1)
    })
})



