// const { Query } = require('mongoose')
const Tour = require('../models/tourModel')
const catchAsync = require('../utils/catchAsync')
const AppError = require('../utils/appError')
const factory = require('./handlerFactory')
// const APIFeatures = require('../utils/apiFeatures')
//Lets Read Data:
// const tours = JSON.parse(fs.readFileSync(`${__dirname}/../dev-data/data/tours-simple.json`))

// exports.checkID = (req, res, next, val) => {
//     // console.log(`Tour id is: ${val}`)
//     if (req.params.id * 1 > tours.length)
//         return res.status(404).json({
//             status: 'fail',
//             message: "Invalid ID"
//         })
//     next()
// }
exports.aliasTopTours = (req, res, next) => {
    req.query.limit = 5
    req.query.sort = '-ratingsAverage,price'
    req.query.fields = 'name,ratingsAverage,price,summary,difficulty'
    next()
}

// exports.getAllTours = catchAsync(async (req, res, next) => {
//     // console.log(req.user)
//     // //BUILD QUERY
//     // // 1)  1A) Filtering 
//     // const queryObj = { ...req.query }
//     // const excludedFields = ['page', 'limit', 'sort', 'fields']
//     // excludedFields.forEach(el => delete queryObj[el])
//     // //   1B) Advanced Filtering 
//     // let queryStr = JSON.stringify(queryObj)
//     // queryStr = queryStr.replace(/\b(gt|gte|lt|lte)\b/g, match => `$${match}`) //gt, gte, lt, lte
//     // queryStr = JSON.parse(queryStr)
//     // let query = Tour.find(queryStr)

//     // 2) Sorting 
//     // if (req.query.sort) {
//     //     const sortBy = req.query.sort.split(',').join(' ')
//     //     query = query.sort(sortBy)
//     // } else {
//     //     query = query.sort('-createdAt')
//     // }
//     // // 3) Fields limiting
//     // if (req.query.fields) {
//     //     const fields = req.query.fields.split(',').join(' ')
//     //     query = query.select(fields)
//     // } else {
//     //     query = query.select('-__v')
//     // }
//     // // 4) Pagination
//     // const page = req.query.page * 1 || 1
//     // const limit = req.query.limit * 1 || 20
//     // const skip = (page - 1) * limit
//     // // page=2&limit=10, 1-10=>pag-1, 11-20=>pag-2, 21-30=>pag-3
//     // query = query.skip(skip).limit(limit)

//     // if (req.query.page) {
//     //     const numTours = await Tour.countDocuments()
//     //     if (skip >= numTours) throw new Error('This page does not exist!')
//     // }
//     //EXECUTE QUERY
//     const features = new APIFeatures(Tour.find(), req.query)
//         .filter()
//         .sort()
//         .limitFields()
//         .paginate()
//     const tours = await features.query

//     // SEND RESPONSE 
//     res.status(200).json({
//         status: 'success',
//         result: tours.length,
//         data: {
//             tours
//         }
//     })
// })

exports.getAllTours = factory.getAll(Tour)
exports.getTour = factory.getOne(Tour, { path: 'reviews' })
exports.createTour = factory.createOne(Tour)
exports.updateTour = factory.updateOne(Tour)
exports.deleteTour = factory.deleteOne(Tour)
// exports.deleteTour = catchAsync(async (req, res, next) => {
//     const tour = await Tour.findByIdAndDelete(req.params.id)
//     if (!tour)
//         return next(new AppError('No tour found with that ID', 404))
//     res.status(204).json({
//         status: 'success',
//         data: null
//     })

// })

exports.getTourStats = catchAsync(async (req, res, next) => {
    const stats = await Tour.aggregate([
        {
            $match: { ratingsAverage: { $gte: 4.5 } }
        },
        {
            $group: {
                // _id: '$difficulty',
                _id: { $toUpper: '$difficulty' },
                numTours: { $sum: 1 },
                numRatings: { $sum: '$ratingsQuantity' },
                avgRating: { $avg: '$ratingsAverage' },
                avgPrice: { $avg: '$price' },
                minPrice: { $min: '$price' },
                maxPrice: { $max: '$price' },
            }
        },
        {
            $sort: { avgPrice: 1 }
        }
        // {
        //     $match: { _id: { $ne: 'EASY' } }
        // }

    ])
    res.status(200).json({
        status: 'success',
        data: {
            stats
        }
    })
})

exports.getMonthlyPlan = catchAsync(async (req, res, next) => {
    const year = req.params.year * 1
    const plan = await Tour.aggregate([
        {
            $unwind: '$startDates'
        },
        {
            $match: {
                startDates: {
                    $gte: new Date(`${year}-01-01`),
                    $lte: new Date(`${year}-12-31`)
                }
            }
        },
        {
            $group: {
                _id: { $month: '$startDates' },
                numTourStarts: { $sum: 1 },
                tours: { $push: '$name' }
            }
        },
        {
            $addFields: { month: '$_id' }
        },
        {
            $project: {
                _id: 0
            }
        },
        {
            $sort: { numTourStarts: -1 }
        },
        {
            $limit: 12
        }
    ])
    res.status(200).json({
        status: 'success',
        data: {
            plan
        }
    })
})

exports.getToursWithin = catchAsync(async (req, res, next) => {
    // '/tours-within/:distance/center/:latlng/unit/:unit'
    // /tours-within/243/center/40,45/unit/mi
    const { distance, latlng, unit } = req.params
    const [lat, lng] = latlng.split(',')
    //mongodb expects the radius is in radians
    //To get radians we divided the ditance by raius of the earth:So
    const radius = unit === 'mi' ? distance / 3963.2 : distance / 6378.1

    if (!lat || !lng)
        return next(new AppError('Please provide latitude and longitude in the format of lat,lng', 400))

    // console.log(distance, lat, lng, unit)
    const tours = await Tour.find({
        startLocation: { $geoWithin: { $centerSphere: [[lng, lat], radius] } }
    })
    res.status(200).json({
        status: 'success',
        results: tours.length,
        data: {
            data: tours
        }
    })
})

exports.getDistances = catchAsync(async (req, res, next) => {
    const { latlng, unit } = req.params
    const [lat, lng] = latlng.split(',')
    if (!lat || !lng)
        return next(new AppError('Please provide latitude and longitude in the format of lat,lng', 400))
    const mutliplier = unit === "mi" ? 0.000621371 : 0.001
    console.log(mutliplier)
    const distances = await Tour.aggregate([
        {
            $geoNear: {
                near: {
                    type: 'Point',
                    coordinates: [lng * 1, lat * 1]
                },
                distanceField: 'distance',
                distanceMultiplier: mutliplier
            }
        },
        {
            $project: {
                distance: 1,
                name: 1,
                ratingsAverage: 1
            }
        }
    ])

    res.status(200).json({
        status: 'success',
        data: {
            data: distances
        }
    })
})