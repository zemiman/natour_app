exports.tourBody = (req, res, next) => {
    // console.log('tour middleware!')
    if (!req.body.name || !req.body.price)
        return res.status(400).json({
            status: 'error',
            data: {

                message: 'name or price is required!'
            }
        })
    next()
}