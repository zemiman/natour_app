const mongoose = require('mongoose')

const Tour = require('./tourModel')
const { findByIdAndDelete, findByIdAndUpdate } = require('./tourModel')

const reviewSchema = new mongoose.Schema({
    review: {
        type: String,
        required: [true, "Review can not be empty"]
    },
    rating: {
        type: Number,
        min: [1, 'min value must graeter than or equal one'],
        max: [5, 'min value must less than or equal five']
    },
    tour: {
        type: mongoose.Schema.ObjectId,
        ref: 'Tour',
        required: [true, 'Review must belong to a tour']
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: [true, 'Review must belong to a user']
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})
//A user can review only once for the same tour:To handle this we use compound index
reviewSchema.index({ tour: 1, user: 1 }, { unique: true })
// reviewSchema.pre(/^find/, function (next) {
//     this.populate({
//         path: 'tour',
//         select: 'name price '
//     }).populate({
//         path: 'user',
//         select: 'name photo'
//     })
//     next()
// })
reviewSchema.pre(/^find/, function (next) {
    this.populate({
        path: 'user',
        select: 'name photo'
    })
    next()
})

// Static functions: in order to get Model to access aggregate(Model.aggregate) 
reviewSchema.statics.calcAverageRatings = async function (tourId) {
    //this key word points to this model
    const stats = await this.aggregate([
        {
            $match: { tour: tourId }
        },
        {
            $group: {
                _id: '$tour',
                nRating: { $sum: 1 },
                avgRating: { $avg: '$rating' }
            }
        }
    ])
    // console.log(stats)
    if (stats.length > 0) {
        await Tour.findByIdAndUpdate(tourId, {
            ratingsQuantity: stats[0].nRating,
            ratingsAverage: stats[0].avgRating
        })
    } else {
        await Tour.findByIdAndUpdate(tourId, {
            ratingsQuantity: 0,
            ratingsAverage: 4.5
        })
    }
}
reviewSchema.post('save', function () {
    //this points to current review docment:
    //static method called Model.method(Review.calcAverageRatings) but we do not have Review
    //So, we use this.constructor.calcAverageRatings instead of Review.calcAverageRatings
    this.constructor.calcAverageRatings(this.tour)
    // next()
})
// findByIdAndDelete
// findByIdAndUpdate
reviewSchema.pre(/^findOneAnd/, async function (next) {
    // To get document we should query a document
    this.rev = await this.findOne()
    // console.log(this.rev)
    next()
})
reviewSchema.post(/^findOneAnd/, async function () {
    // await this.findOne():Does not work here, the query has already executed...
    await this.rev.constructor.calcAverageRatings(this.rev.tour)
})
const Review = mongoose.model('Review', reviewSchema)
module.exports = Review