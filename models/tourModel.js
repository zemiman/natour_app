const mongoose = require('mongoose')
const slugify = require('slugify')
// const validator = require('validator')

// const User = require('./userModel')
//Schema for tour source:
const tourSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A tour must have a name!"],
        unique: true,
        trim: true,
        // maxlength: [40, 'A tour must have less or equal than 40 characters'],
        // minlength: [8, 'A tour must have more or equal than 8 characters']
        // validate: [validator.isAlpha, 'Tour name must only contain charaters']
    },
    slug: String,
    duration: {
        type: Number,
        required: [true, 'A tour must have a duration']
    },
    maxGroupSize: {
        type: Number,
        required: [true, "A tour must have a group size"]
    },
    difficulty: {
        type: String,
        required: [true, "A tour must have a difficulty"],
        enum: {
            values: ['easy', 'medium', 'difficult'],
            message: 'Difficulty is either: easy, medium, difficult'
        }
    },
    ratingsAverage: {
        type: Number,
        default: 4.5,
        min: [1, "Rating must be above 1.0"],
        max: [5, "Rating must be below 5.0"],
        set: val => Math.round(val * 10) / 10 //4.876=>5 but 4.876*10=48.76=>49/10=4.9
    },
    ratingsQuantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        required: [true, "A tour must have a price"]
    },
    priceDiscount: {
        type: Number,
        validate: {
            validator: function (val) {
                //this only points to current doc on NEW document creation:
                return val < this.price
            },
            message: 'Discount price ({VALUE}) should be below regural price!'
        }
    },
    summary: {
        type: String,
        trim: true,
        required: [true, "A tour must have a summary"]
    },
    description: {
        type: String,
        trim: true
    },
    imageCover: {
        type: String,
        require: [true, "A tour must have a cover image"]
    },
    images: [String],
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    },
    startDates: [Date],
    secretTour: {
        type: Boolean,
        default: false
    },
    startLocation: {
        type: {
            type: String,
            default: 'Point',
            enum: ['Point']
        },
        coordinates: [Number],
        address: String,
        description: String
    },
    locations: [
        {
            type: {
                type: String,
                default: 'Point',
                enum: ['Point']
            },
            coordinates: [Number],
            address: String,
            description: String,
            day: Number
        }
    ],
    guides: [
        {
            type: mongoose.Schema.ObjectId,
            ref: 'User'
        }
    ]
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})
///Create Indexes
// tourSchema.index({ price: 1 }) // single index
tourSchema.index({ slug: 1 }) // single index
tourSchema.index({ price: 1, ratingsAverage: -1 }) // compound index
//To tell mongodb w/c field used in geo spatial query
tourSchema.index({ startLocation: '2dsphere' }) //2D sphere index 
//Virtual Property:
tourSchema.virtual('durationWeeks').get(function () {
    return this.duration / 7
})
// Virtual Populate 
tourSchema.virtual('reviews', {
    ref: 'Review',
    foreignField: 'tour',
    localField: '_id'
})

//DOCUMENT MIDDLEWARE: runs before .save() and .create(), but not .insertMany()
tourSchema.pre('save', function (next) {
    this.slug = slugify(this.name, { lower: true })
    next()
})

// tourSchema.pre('save', async function (next) {
//     if (this.guides.length === 0) return next()
//     const guidesPromise = this.guides.map(async id => await User.findById(id))
//     this.guides = await Promise.all(guidesPromise)
//     next()
// })
// tourSchema.pre('save', function (next) {
//     console.log('Doc will save...')
//     next()
// })
// tourSchema.post('save', function (doc, next) {
//     console.log(doc)
//     next()
// })
//QUERY MIDDLEWARE:
// tourSchema.pre('find', function (next) {
// tourSchema.pre(/^find/, function (next) {
//     this.find({ secretTour: { $ne: true } })
//     this.start = Date.now()
//     next()
// })

// tourSchema.post(/^find/, function (docs, next) {
//     console.log(`Query took:${Date.now() - this.start} milliseconds`)
//     next()
// })
tourSchema.pre(/^find/, function (next) {
    // if (this.all) return next()

    this.populate({
        path: 'guides',
        select: '-__v -passwordChangedAt'
    })
    next()
})
//AGGREGATION MIDDLEWARE:
// tourSchema.pre('aggregate', function (next) {
//     this.pipeline().unshift({ $match: { secretTour: { $ne: true } } })
//     // console.log(this.pipeline())
//     next()
// })
//Create Tour Model:
const Tour = mongoose.model('Tour', tourSchema)
module.exports = Tour