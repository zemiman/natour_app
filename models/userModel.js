const crypto = require('crypto')
const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, "Please tell us your name!"]
    },
    // lastName: {
    //     type: String,
    //     require: [true, "User must have a last name!"]
    // },
    email: {
        type: String,
        require: [true, "Please provide your email!"],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide a valid email!"]
    },
    photo: String,
    role: {
        type: String,
        enum: {
            values: ['user', 'guide', 'lead-guide', 'admin'],
            message: "Role is either:user, guide, lead-guide, admin"
        },
        default: 'user'
    },
    password: {
        type: String,
        require: [true, "Please provide a password!"],
        minlength: [6, "password contains at least six characters!"],
        select: false

    },
    passwordConfirm: {
        type: String,
        require: [true, "Please confirm your password!"],
        validate: {
            //This only works on CREATE and SAVE!!
            validator: function (val) {
                return val === this.password
            },
            message: "passwords are not the same!"
        }
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpires: Date,
    active: {
        type: Boolean,
        default: true,
        select: false
    }
})
// Encrypt password  
userSchema.pre('save', async function (next) {
    //Only run this function if password was actually modified
    if (!this.isModified('password')) return next()
    //Generate salt
    // const salt = await bcrypt.genSalt(12)
    // this.password=bcrypt.hash(this.password, salt)
    //Hash password with the cost of 12
    this.password = await bcrypt.hash(this.password, 12)
    //Delete passwordConfirm field
    this.passwordConfirm = undefined
    next()
})

userSchema.pre('save', function (next) {
    if (!this.isModified('password') || this.isNew) return next()
    this.passwordChangedAt = Date.now() - 1000;
    next()
})

userSchema.pre(/^find/, function (next) {
    //this points to the current query:
    this.find({ active: { $ne: false } })
    next()
})
//Instance method=>functions exists at every document:
userSchema.methods.correctPassword = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword)
}
userSchema.methods.changedPasswordAfter = function (JWTTimestamp) {
    if (this.passwordChangedAt) {
        const changedTimestamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10)
        // console.log(changedTimestamp, JWTTimestamp)
        //Return true means password is changed:
        return JWTTimestamp < changedTimestamp
    }
    return false
}
userSchema.methods.createPasswordResetToken = function () {
    const resetToken = crypto.randomBytes(32).toString('hex')
    this.passwordResetToken = crypto.createHash('sha256').update(resetToken).digest('hex')
    // console.log({ resetToken }, this.passwordResetToken)
    this.passwordResetExpires = Date.now() + 10 * 60 * 1000
    return resetToken
}
const User = mongoose.model('User', userSchema)
module.exports = User