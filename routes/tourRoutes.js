const express = require('express')
const router = express.Router()

const tourController = require('../controllers/tourController')
const authController = require('../controllers/authController')
const reviewRouter = require('./reviewRoutes')

// const tourMiddleware = require('../middlewares/tourMiddleware')

// Param Middleware to chech tour id before any controller hit:
// router.param('id', tourController.checkID)

//POST tour/8et04/reviews
//GET tour/8et04/reviews
router.use('/:tourId/reviews', reviewRouter)

//Alias route for top 5 cheapest
router.route('/top-5-cheap').get(tourController.aliasTopTours, tourController.getAllTours)
router.route('/tour-stats').get(tourController.getTourStats)
router.route('/monthly-plan/:year')
    .get(
        authController.protect,
        authController.restrictTo('admin', 'lead-guide', 'guide'),
        tourController.getMonthlyPlan
    )

router.route('/tours-within/:distance/center/:latlng/unit/:unit').get(tourController.getToursWithin)
// /tours-within/243/center/40,45/unit/mi

//Calculate distance to each startLocations of a tour:
router.route('/distances/:latlng/unit/:unit').get(tourController.getDistances)

router.route('/')
    .get(tourController.getAllTours)
    .post(
        authController.protect,
        authController.restrictTo('admin', 'lead-guide'),
        tourController.createTour
    )
router.route('/:id')
    .get(tourController.getTour)
    .patch(
        authController.protect,
        authController.restrictTo('admin', 'lead-guide'),
        tourController.updateTour
    )
    .delete(
        authController.protect,
        authController.restrictTo('admin', 'lead-guide'),
        tourController.deleteTour
    )


module.exports = router