const express = require('express')
const morgan = require('morgan')
const rateLimit = require('express-rate-limit')
const helmet = require('helmet')
const mongoSanitize = require('express-mongo-sanitize')
const xss = require('xss-clean')
const hpp = require('hpp')

const app = express()

//Import Route modules:
const globaleErrorHandler = require('./controllers/errorController')
const AppError = require('./utils/appError')
const tourRouter = require('./routes/tourRoutes')
const userRouter = require('./routes/userRoutes')
const reviewRouter = require('./routes/reviewRoutes')

//1) MIDDLEWARES:
//Set security http headers:
app.use(helmet())
//Development logging:

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}
const limiter = rateLimit({
    max: 100,
    windowMs: 90 * 60 * 1000,
    message: "Too many requests from this IP, please try again after an hour and half."
})
//Limit requests from same IP:
app.use('/api', limiter)
//Body parser, reading data from body to req.body
app.use(express.json({ limit: '10kb' }))
//Data sanitization against NOSQL query injection
app.use(mongoSanitize()) //cleans all $ and dots in req.body data
//Data sanitization against XSS
app.use(xss()) //Cleans any user input from malicious html code:
//Prevent parameter pollution:hpp=>http parameter pollution
app.use(hpp({
    whitelist: [
        "duration",
        "ratingsAverage",
        "ratingsQuantity",
        "maxGroupSize",
        "difficulty",
        "price"

    ]
}))
//Serving static files
app.use(express.static(`${__dirname}/public`))
// My own Middleware: or Test middleware
app.use((req, res, next) => {
    // console.log(req.headers)
    req.requestTime = new Date().toISOString()
    next()
})


// 3) ROUTES
app.use('/api/v1/tours', tourRouter)
app.use('/api/v1/users', userRouter)
app.use('/api/v1/reviews', reviewRouter)

// HANDLE UNMATCHED URL:
app.all('*', (req, res, next) => {
    next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404))
})

app.use(globaleErrorHandler)

module.exports = app